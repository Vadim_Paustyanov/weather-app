import 'package:flutter/material.dart';

Widget HouryWeather(
    String temp1,
    String temp2,
    String temp3,
    String temp4,
    String temp5,
    String temp6,
    String temp7,
    String icon1,
    String icon2,
    String icon3,
    String icon4,
    String icon5,
    String icon6,
    String icon7,
    String date1,
    String date2,
    String date3,
    String date4,
    String date5,
    String date6,
    String date7,
    String time1,
    String time2,
    String time3,
    String time4,
    String time5,
    String time6,
    String time7) {
  return Column(
    children: [
      const Text("Hourly Weather",
          style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: Color.fromARGB(255, 0, 0, 0),
              fontFamily: "Montserrat")),
      const SizedBox(height: 10),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date1,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time1,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon1.png",
                    fit: BoxFit.fill)),
            Text("$temp1°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ])
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date2,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time2,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon2.png",
                    fit: BoxFit.fill)),
            Text("$temp2°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ])
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date3,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time3,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon3.png",
                    fit: BoxFit.fill)),
            Text("$temp3°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ]),
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date4,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time4,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon4.png",
                    fit: BoxFit.fill)),
            Text("$temp4°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ]),
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date5,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time5,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon5.png",
                    fit: BoxFit.fill)),
            Text("$temp5°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ]),
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date6,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time6,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon6.png",
                    fit: BoxFit.fill)),
            Text("$temp6°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ]),
        ],
      ),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 1,
          indent: 30,
          endIndent: 30),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            Text(date7,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(time7,
                style: const TextStyle(
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 20))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
                width: 80,
                child: Image.network(
                    "http://openweathermap.org/img/w/$icon7.png",
                    fit: BoxFit.fill)),
            Text("$temp7°C",
                style: const TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Color.fromARGB(255, 19, 0, 187)))
          ]),
        ],
      )
    ],
  );
}
