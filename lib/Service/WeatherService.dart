import 'dart:convert';
import 'package:http/http.dart' as http;

class WeatherService {
  Future<CurrentWeather>? getCurrentWeather(String? location) async {
    var response = await http.get(Uri.parse(
        "https://api.openweathermap.org/data/2.5/weather?q=$location&appid=8f80c26e2163ac0e35ebc42f9a79e9a7&units=metric"));
    var body = jsonDecode(response.body);
    return CurrentWeather.fromJson(body);
  }

  Future<HourlyWeather>? getDailyWeather(String? location) async {
    var response2 = await http.get(Uri.parse(
        "https://api.openweathermap.org/data/2.5/forecast?q=$location&cnt=7&appid=8f80c26e2163ac0e35ebc42f9a79e9a7&units=metric"));
    var body2 = jsonDecode(response2.body);
    return HourlyWeather.fromJson(body2);
  }
}

class CurrentWeather {
  int? temp;
  String? city;
  double? wind;
  int? feelsLike;
  int? humidity;
  int? pressure;
  String? icon;

  CurrentWeather(
      {this.temp,
      this.city,
      this.wind,
      this.feelsLike,
      this.humidity,
      this.pressure,
      this.icon});

  CurrentWeather.fromJson(Map<String, dynamic> json) {
    temp = json["main"]["temp"].round();
    city = json["name"];
    wind = json["wind"]["speed"];
    feelsLike = json["main"]["feels_like"].round();
    humidity = json["main"]["humidity"];
    pressure = json["main"]["pressure"];
    icon = json["weather"][0]["icon"];
  }
}

class HourlyWeather {
  int? temp1;
  int? temp2;
  int? temp3;
  int? temp4;
  int? temp5;
  int? temp6;
  int? temp7;
  String? icon1;
  String? icon2;
  String? icon3;
  String? icon4;
  String? icon5;
  String? icon6;
  String? icon7;
  String? date1;
  String? date2;
  String? date3;
  String? date4;
  String? date5;
  String? date6;
  String? date7;
  String? time1;
  String? time2;
  String? time3;
  String? time4;
  String? time5;
  String? time6;
  String? time7;

  HourlyWeather(
      {this.temp1,
      this.temp2,
      this.temp3,
      this.temp4,
      this.temp5,
      this.temp6,
      this.temp7,
      this.icon1,
      this.icon2,
      this.icon3,
      this.icon4,
      this.icon5,
      this.icon6,
      this.icon7,
      this.date1,
      this.date2,
      this.date3,
      this.date4,
      this.date5,
      this.date6,
      this.date7,
      this.time1,
      this.time2,
      this.time3,
      this.time4,
      this.time5,
      this.time6,
      this.time7});

  HourlyWeather.fromJson(Map<String, dynamic> json) {
    temp1 = json["list"][0]["main"]["temp"].round();
    temp2 = json["list"][1]["main"]["temp"].round();
    temp3 = json["list"][2]["main"]["temp"].round();
    temp4 = json["list"][3]["main"]["temp"].round();
    temp5 = json["list"][4]["main"]["temp"].round();
    temp6 = json["list"][5]["main"]["temp"].round();
    temp7 = json["list"][6]["main"]["temp"].round();
    icon1 = json["list"][0]["weather"][0]["icon"];
    icon2 = json["list"][1]["weather"][0]["icon"];
    icon3 = json["list"][2]["weather"][0]["icon"];
    icon4 = json["list"][3]["weather"][0]["icon"];
    icon5 = json["list"][4]["weather"][0]["icon"];
    icon6 = json["list"][5]["weather"][0]["icon"];
    icon7 = json["list"][6]["weather"][0]["icon"];
    String string1 = json["list"][0]["dt_txt"];
    final splitted1 = string1.split(' ');
    date1 = splitted1[0];
    time1 = splitted1[1];
    String string2 = json["list"][1]["dt_txt"];
    final splitted2 = string2.split(' ');
    date2 = splitted2[0];
    time2 = splitted2[1];
    String string3 = json["list"][2]["dt_txt"];
    final splitted3 = string3.split(' ');
    date3 = splitted3[0];
    time3 = splitted3[1];
    String string4 = json["list"][3]["dt_txt"];
    final splitted4 = string4.split(' ');
    date4 = splitted4[0];
    time4 = splitted4[1];
    String string5 = json["list"][4]["dt_txt"];
    final splitted5 = string5.split(' ');
    date5 = splitted5[0];
    time5 = splitted5[1];
    String string6 = json["list"][5]["dt_txt"];
    final splitted6 = string6.split(' ');
    date6 = splitted6[0];
    time6 = splitted6[1];
    String string7 = json["list"][6]["dt_txt"];
    final splitted7 = string7.split(' ');
    date7 = splitted7[0];
    time7 = splitted7[1];
  }
}
