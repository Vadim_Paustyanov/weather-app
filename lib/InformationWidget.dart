import 'package:flutter/material.dart';

Widget AdditionalInformation(String wind, String humidity, String pressure) {
  return Column(
    children: [
      const SizedBox(height: 25),
      const Text("Additional Information",
          style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: Color.fromARGB(255, 0, 0, 0),
              fontFamily: "Montserrat")),
      const SizedBox(height: 25),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(children: [
            const Text("Wind",
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat")),
            Text(wind,
                style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat"))
          ]),
          Column(children: [
            const Text("Humidity",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat")),
            Text(humidity,
                style: const TextStyle(
                    fontSize: 20.0,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat"))
          ]),
          Column(children: [
            const Text("Pressure",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat")),
            Text(pressure,
                style: const TextStyle(
                    fontSize: 20.0,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Montserrat"))
          ])
        ],
      ),
      const SizedBox(height: 25),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 2,
          indent: 25,
          endIndent: 25),
      const SizedBox(height: 25),
    ],
  );
}
