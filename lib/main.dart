import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:app/WeatherWidget.dart';
import 'package:app/InformationWidget.dart';
import 'package:app/Service/WeatherService.dart';
import 'package:app/HourlyWidget.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _textController = TextEditingController();
  String location = "London";

  WeatherService client = WeatherService();
  CurrentWeather? data;
  HourlyWeather? hourlyData;

  Future<void> getData() async {
    data = await client.getCurrentWeather(location);
    hourlyData = await client.getDailyWeather(location);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          elevation: 0.0,
          title: const Text(
            "Weather App",
            style: TextStyle(
                color: Color.fromARGB(255, 19, 0, 187),
                fontFamily: "Montserrat",
                fontWeight: FontWeight.bold,
                fontSize: 30),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: FutureBuilder(
                future: getData(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Column(children: [
                      Container(
                        alignment: Alignment.center,
                        child: SizedBox(
                          width: 350,
                          child: TextField(
                            controller: _textController,
                            decoration: InputDecoration(
                              hintText: "City",
                              border: const OutlineInputBorder(),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    location = _textController.text;
                                  });
                                },
                                color: Colors.black,
                                icon: const Icon(Icons.search),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Weather("${data?.icon}", "${data?.temp}", "${data?.city}",
                          "${data?.feelsLike}"),
                      AdditionalInformation("${data?.wind}",
                          "${data?.humidity}", "${data?.pressure}"),
                      HouryWeather(
                          "${hourlyData?.temp1}",
                          "${hourlyData?.temp2}",
                          "${hourlyData?.temp3}",
                          "${hourlyData?.temp4}",
                          "${hourlyData?.temp5}",
                          "${hourlyData?.temp6}",
                          "${hourlyData?.temp7}",
                          "${hourlyData?.icon1}",
                          "${hourlyData?.icon2}",
                          "${hourlyData?.icon3}",
                          "${hourlyData?.icon4}",
                          "${hourlyData?.icon5}",
                          "${hourlyData?.icon6}",
                          "${hourlyData?.icon7}",
                          "${hourlyData?.date1}",
                          "${hourlyData?.date2}",
                          "${hourlyData?.date3}",
                          "${hourlyData?.date4}",
                          "${hourlyData?.date5}",
                          "${hourlyData?.date6}",
                          "${hourlyData?.date7}",
                          "${hourlyData?.time1}",
                          "${hourlyData?.time2}",
                          "${hourlyData?.time3}",
                          "${hourlyData?.time4}",
                          "${hourlyData?.time5}",
                          "${hourlyData?.time6}",
                          "${hourlyData?.time7}")
                    ]);
                  }
                  return Container();
                })));
  }
}
