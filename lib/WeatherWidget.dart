import 'package:flutter/material.dart';

Widget Weather(String icon, String temp, String city, String feelsLike) {
  return Column(
    children: [
      SizedBox(height: 70),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
              width: 150,
              child: Image.network("http://openweathermap.org/img/w/$icon.png",
                  fit: BoxFit.fill)),
          Column(
            children: [
              Text(
                "$temp°C",
                style: const TextStyle(
                    fontSize: 60.0,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 19, 0, 187),
                    fontFamily: "Montserrat"),
              ),
              Text(
                "Feels like: $feelsLike °C",
                style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 117, 117, 117),
                    fontFamily: "Montserrat"),
              )
            ],
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.location_on,
              size: 27.0, color: Color.fromARGB(255, 117, 117, 117)),
          Text(
            city,
            style: const TextStyle(
                fontSize: 27.0,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 117, 117, 117),
                fontFamily: "Montserrat"),
          )
        ],
      ),
      SizedBox(height: 70),
      Divider(
          color: Color.fromARGB(255, 117, 117, 117),
          thickness: 2,
          indent: 25,
          endIndent: 25)
    ],
  );
}
